//#include <SoftwareSerial.h>
//const int TxPin = 0;
//SoftwareSerial mySerial = SoftwareSerial(255, TxPin);

int clockPin = 4; //yellow
int latchPin = 2; //red 
int dataPin = 1; //black

int heatPin = 0; 
int potPin = 3;

void setup() 
{
  //pinMode(TxPin, OUTPUT);
  //digitalWrite(TxPin, HIGH);
    
  //mySerial.begin(9600);
  //mySerial.println("Reset...");
  
  pinMode(latchPin, OUTPUT);
  pinMode(dataPin, OUTPUT);  
  pinMode(clockPin, OUTPUT);
}
 

void loop() 
{ 
  int pot = getPot();
  setDigit(pot);
  setHeat(pot);
  
  //mySerial.print(analogRead(3));
  //mySerial.print(" ");
  //mySerial.println(pot);

}


int getPot(){
  int v = analogRead(potPin);
  
  if(v >= 173 && v < 184)
    return 1;
  else if(v >= 184 && v < 195)
    return 2;
  else if(v >= 195 && v < 205)
    return 3;
  else if(v >= 205 && v < 220)
    return 4;
  else if(v >= 220 && v < 240)
    return 5;
  else if(v >= 240 && v < 260)
    return 6;
  else if(v >= 260 && v < 290)
    return 7;
  else if(v >= 290 && v < 325)
    return 8;
  else if(v >= 325 && v < 360)
    return 9;
  else if(v >= 360 && v < 410)
    return 10;
  else if(v >= 410 && v < 490)
    return 11;
  else if(v >= 490 && v < 575)
    return 12;
  else if(v >= 575 && v < 700)
    return 13;
  else if(v >= 700 && v < 900)
    return 14;
  else if(v >= 900 && v < 1100)
    return 15;
  else return 0;
    
}



void setHeat(int level)
{
   int l = 0;
   for(int i = 0; i < level; i++)
   {
     l = i * level;
   }
   analogWrite(heatPin, l); 
}

void setDigit(int dig){
  // Describe each digit in terms of display segments
  // 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, A, B, C, D, E, F
  const byte numbers[16] = {
  /*
   _     6
  | |    4 5
   _     3
  | |    2 1
   _ .   7 0
  */
                    0b01101111, //0
                    0b01000100, //1
                    0b00110111, //2
                    0b01010111, //3
                    0b01011100, //4
                    0b01011011, //5
                    0b01111011, //6
                    0b01000110, //7
                    0b01111111, //8
                    0b01011110, //9
                    0b01111110, //A
                    0b01111001, //b
                    0b00110001, //c
                    0b01110101, //d
                    0b00111011, //E
                    0b00111010  //F
  };
  digitalWrite(latchPin, LOW);
  shiftOut(dataPin, clockPin, LSBFIRST, numbers[dig]);
  digitalWrite(latchPin, HIGH);
}

